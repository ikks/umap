from django.contrib import messages
from django.utils.translation import ugettext

from osm_backend import LackOsmPermissions

from social_auth.middleware import SocialAuthExceptionMiddleware
from social_auth.exceptions import AuthCanceled


class CustomSocialAuthExceptionMiddleware(SocialAuthExceptionMiddleware):
    """Error management on failed user authentication"""
    def get_message(self, request, exception):
        if isinstance(exception, AuthCanceled):
            messages.error(request, ugettext('Not logged in'))
        elif isinstance(exception, LackOsmPermissions):
            messages.error("{0}".exception)
